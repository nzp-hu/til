# Convert softsubbed mkv to hardsubbed mp4

Quick snippet:
```
ffmpeg -i in.mkv -vf subtitles=in.mkv:si=0 -c:v libx264 -c:a copy -map 0:v -map 0:a:0 out.mp4
```

The subtitles filter renders subs onto the video stream.

Argument descriptions:

 | Argument       | Description                                                                                                                               |
 | -------------- | ----------------------------------------------------------------------------------------------------------------------------------------- |
 | `si`           | The stream index of the subtitle stream from `in.mkv` to use. `0` is for the first subtitle stream, `1` for the 2nd and so on.            |
 | `-c:v libx264` | Tells ffmpeg to re-encode any video streams mapped as H.264 using x264.                                                                   |
 | `-c:a`         | Copy means any audio streams mapped should just be copied instead of re-encoded.                                                          |
 | `-map 0:v`     | Tells ffmpeg to include the (processed) video stream along with the 1st audio stream. Change `0:a:0` to `0:a:1` for the 2nd audio stream. |


[Source](https://old.reddit.com/r/ffmpeg/comments/5senc9/how_to_convert_softsubbed_mkv_to_hardsubbed_mp4/)