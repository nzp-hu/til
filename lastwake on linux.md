# Reason for last wake on Linux

Was looking for the equivalent of `powercfg /lastwake` on Windows.

```shell
sudo dmidecode | grep Wake-up
```

Example output:
>Wake-up Type: Power Switch

More options: https://unix.stackexchange.com/questions/132115/how-to-find-reason-a-notebook-wakes-upb