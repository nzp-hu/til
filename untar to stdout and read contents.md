# Pipe file from tar archive to a command

Quick snippet:
```
tar --to-stdout -xf pihole.tar 7351a4bb62c66d7cfd3eb271a07da5493a22797c28788fd91cd6285825cf75d2.json | jq '.architecture'
```

Argument descriptions:

| Argument              | Description                                       |
| --------------------- | ------------------------------------------------- |
| `-x`                  | extract                                           |
| `-f`                  | file                                              |
| `--to-stdout`         | push the extracted file to stdout                 |
| `pihole.tar`          | archive filename                                  |
| `7351a4bb6[...].json` | file in the archive that you'd like to extract    |
| `jq '.architecture'`  | read the value of architecture from the json file |
