# Convert audio to mp3 via ffmpeg

```shell
ffmpeg -i input.wav -acodec mp3 output.mp3
```

...or define bitrate by using the `-ab` argument:

```shell
ffmpeg -i input.wav -acodec mp3 -ab 64k output.mp3
```